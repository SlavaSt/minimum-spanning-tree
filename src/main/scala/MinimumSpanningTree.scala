import scala.collection.mutable
import scala.math.Ordering

object MinimumSpanningTree {

  def mst(graph: Graph): Graph = {
    def findMst: Graph = {
      val result = new Graph(graph.nodeCount)
      val queue = new mutable.PriorityQueue[Edge]()(Ordering.by {
        edge: Edge => edge.weight
      }.reverse)
      val marked: Array[Boolean] = Array.fill(graph.nodeCount + 1)(false)
      queue.enqueue(graph.adj(1): _*)
      marked(1) = true

      while (queue.nonEmpty) {
        val minEdge = queue.dequeue()
        if (!marked(minEdge.nodeTo)) {
          result.addEdge(minEdge)
          marked(minEdge.nodeTo) = true
          queue.enqueue(graph.adj(minEdge.nodeTo): _*)
        }
      }

      result
    }

    if (graph.nodeCount == 0) new Graph(0)
    else findMst
  }

  def overallMstCost(graph: Graph): Int = {
    val minSpanningTree = mst(graph)
    minSpanningTree.edges().map(_.weight).sum
  }
}
