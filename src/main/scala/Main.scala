import scala.io.Source

/**
 * @author Slava Stashuk
 */
object Main extends App {

  val input = "edges.txt"

  val lines = Source.fromFile(getClass.getResource(input).toURI).getLines().toStream

  val firstLine = lines.head
  val tuple = firstLine.split(" ")

  val nodesNum = tuple(0).toInt
  val edgesNum = tuple(1).toInt

  val graph = new Graph(nodesNum)

  for (line <- lines.tail) {
    val tuple = line.split(" ")

    val nodeFrom = tuple(0).toInt
    val nodeTo = tuple(1).toInt
    val weight = tuple(2).toInt
    graph.addEdge(Edge(nodeFrom, nodeTo, weight))
    graph.addEdge(Edge(nodeTo, nodeFrom, weight))
  }

  assertNoDuplicates(MinimumSpanningTree.mst(graph))
  println(MinimumSpanningTree.mst(graph))
  println(MinimumSpanningTree.overallMstCost(graph))



  def assertNoDuplicates(graph:Graph) = {
    val edgeList = graph.edges().map(edge => Set(edge.nodeFrom, edge.nodeTo)).toList
    assert (edgeList.size == edgeList.toSet.size)
  }


}
