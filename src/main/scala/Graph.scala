
class Graph(nodeNum: Int) {

  private val adjList: Array[List[Edge]] = Array.fill(nodeNum + 1){ List() }

  def adj(node: Int): List[Edge] = {
    adjList(node)
  }

  def edges(): List[Edge] = {
    adjList.flatten.toList
  }

  def nodeCount = nodeNum

  def addEdge(edge: Edge) = {
    val edgeList = adjList(edge.nodeFrom)
    adjList.update(edge.nodeFrom, edge :: edgeList)
  }


  override
  def toString = adjList.mkString("\n")


}
